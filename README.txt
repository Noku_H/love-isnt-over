How to set up and use version control using commandline!
It may sound scary but I promise it's not as bad as you'd think

First of all make sure you have a gitlab account and are invited to the woe.begame development group

_Getting started_
1. Create a folder where you want the repository. for example E:\woe.begame\LoveIsntOver  
Everything from the git repository will go into this folder
2. Open a terminal or PowerShell 
3. Type the following into your terminal 
	cd "E:\woe.begame\LoveIsntOver"		(replace the path in the parentheses with the full path to your folder)
	git init 
	git remote add origin https://gitlab.com/woe.begame-development/love-isnt-over.git
	git pull origin main

At this point your folder should have a few new files and folders in it! If not, feel free to ask for help and I'll help you troubleshoot
(Any time it says to type something into the terminal I am assuming you are in the correct location)
(use cd "E:\woe.begame\LoveIsntOver" in the terminal if needed)


_Making branches_
Now we are going to set up a local branch for you, so you don't push your changes straight into the main branch!

1. Create a random file somewhere in your local repository. An empty .txt file is good. We'll use this to make sure everything works!
2. Type in the terminal
	git checkout -b YOUR_BRANCH_NAME 
This creates and checks out a new branch named YOUR_BRANCH_NAME (name it something like your nickname or the feature you're working on). 


First time committing you may get an error starting with ***PLease tell me who you are
If you get it type the commands the terminal tells you to and try again!

_Pushing changes_
	git add .
This adds your changes (the empty .txt you just made) to the "staging area", from which you can commit and push it
add . adds ALL changes, you can add only some changes by specifying add FILENAME (for example git add README.txt would add only README.txt)
	git commit -m "COMMENT"
This commits your staged changes. replace COMMENT with a short description of your change (for example "my first test commit")
	git push --set-upstream origin YOUR_BRANCH_NAME
This finally pushes your changes to the git repository! You should now see your branch on the gitlab website under Repository->Branches
Pushing changes after your first time you only need to type git push

When you try to push the first time it might ask you to sign into gitlab. do that with the window that pops up!

You should do all your work in a branch, and when the feature (some code, writing, art, etc) is ready to go into the project you can
create a Merge Request by pressing the "Merge request" button


_Merge requests_
You've pushed your changes and now you want to put them in the main branch. Press the Merge request button

1. Check that the from/to branches are correct (from YOUR_BRANCH_NAME into main)
2. Give your merge request a short but descriptive title. For example "Add more sprites for Mike Walters".
Generally there are styleguides on how to do this but as long as it's decent it's fine

3. Write a longer description if needed. Might not be needed if the title and changelog tells you all you need, 
but for bigger merge requests you should do this
4. Leave both Merge options unchecked 
5. Create Merge Request

Now let a project owner know about your merge request so they can merge it for you! (we will probably all be owners but who knows)


_Updating your local repository_
If any changes have been made to the local repository you have to update your local repository to get the changes
Make sure you have no uncommitted local changes before doing this (remove changes or commit them)
Type in the terminal
	git fetch
This gets the changes from the remote repository so you can pull them into your local branches
	git pull
This finally updates your local files!


_Merge conflicts_
If you try to pull  the repository but you have local changes to a file that's also changed in the repository you will get a merge conflict. 
TODO figure out how to solve them and write down how to here :)


_Useful stuff_
	git status
Lets you know a bunch of useful information, like what local branch you are on, which remote branch you are connected to, 
differences between them
	git checkout YOUR_BRANCH_NAME 
Checks out local branch without creating new
	git branch --set-upstream-to=origin/YOUR_BRANCH_NAME 
This command connects your local branch to an existing remote branch
